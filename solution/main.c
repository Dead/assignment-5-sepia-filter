#include "bmp_format.h"
#include "img_transform.h"
#include "input_formats.h"
#include <stdio.h>

int main( int argc, char** argv ) {
//    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc < 3){
        printf("Not all args passed\n");
        return 1;
    }
    FILE *fp;
    FILE *out;

    if ((fp = fopen(argv[1], "r")) == NULL){
        printf("Cannot open file to read\n");
        return 1;
    }   
    
    if ((out = fopen(argv[2], "w")) == NULL){
        printf("Cannot open file to write\n");
        return 1;
    }

    struct image img = create_empty_image();

    enum read_status read_status = from_bmp(fp, &img);
    if (read_status != READ_OK){
        printf("Program ended with read error");
        return 1;
    }


    struct image rotated = rotate(&img);
    enum write_status write_status = to_bmp(out, &rotated);
    if (write_status != WRITE_OK){
        printf("Program ended with write error");
        return 1;
    }
    
    free(img.data);
    free(rotated.data);
    // delete_image(&img);
    // delete_image(&rotated);
    fclose(fp);
    fclose(out);
    return 0;
}
