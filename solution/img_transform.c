#include "img_worker.h"
#include <stdint.h>


struct image rotate(struct image* img){
    struct image new_img = create_image(img->height, img->width);
    for (uint64_t i = 0; i < new_img.height; ++i ){
        for(uint64_t j =0; j < new_img.width; ++j){
            new_img.data[i* new_img.width + j] = img->data[(img->height - j - 1) * img->width + i];
        }
    }
    return new_img;
}
