#ifndef IMG_TRANSFORMATOR_H
#define IMG_TRANSFORMATOR_H

#include "img_worker.h"


/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image* img);

#endif

